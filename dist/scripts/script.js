
document.addEventListener('DOMContentLoaded', function() {
    const inputs = document.querySelectorAll('.contactForm_input');
    function validatorInput(input, asterisk){
        input.value !== '' ? asterisk.style.display = 'none': asterisk.style.display = 'inline'
    }

    inputs.forEach(input => {
        const asterisk = input.parentElement.querySelector('.contactForm_asterisk');
        
    validatorInput(input, asterisk)       

       
        input.addEventListener('input', function() {
            validatorInput(input, asterisk)
        });

        input.addEventListener('blur', function() {
            if (input.value === '') {
                asterisk.style.display = 'inline'; 
            }
        });
    });
});

const ourProjects = document.querySelectorAll(".projects_gallary--item");
const allProjectText = document.querySelectorAll(".projects_gallary--project-name");


ourProjects.forEach(project => {
    project.addEventListener("mouseover", function(event) {
       
    allProjectText.forEach(project => project.classList.remove("active"));
        const selectedProject = event.currentTarget;
        const projectText = selectedProject.querySelector(".projects_gallary--project-name");
       
        if (projectText) {
            projectText.classList.add("active");
           
        }
    });
});